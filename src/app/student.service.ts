import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class StudentService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization' : 'Bearer ' + sessionStorage.getItem('accessToken')
    })
  };

  apiUrlGetProfile : string = "http://localhost:8080/secure/profile";
  apiUrlUpdateProfile : string = "http://localhost:8080/secure/update/student";

  getProfile(): Observable<any>{

    console.log('Token: ' + this.httpOptions.headers.get('Authotization'));
    return this.httpClient.get<any>(this.apiUrlGetProfile, this.httpOptions);
  }

  updateProfile(studentDetails, id): Observable<any>{
    return this.httpClient.post(`http://localhost:8080/secure/update/student/${id}`, studentDetails, this.httpOptions);
  }

  constructor(private httpClient: HttpClient) { }
}


