import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-admin-deg-req-courses-list',
  templateUrl: './admin-deg-req-courses-list.component.html',
  styleUrls: ['./admin-deg-req-courses-list.component.css']
})
export class AdminDegReqCoursesListComponent implements OnInit {

  @Input() requiredCourses = [];

  constructor() { }

  ngOnInit() {
  }

}
