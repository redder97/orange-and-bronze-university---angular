import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDegReqCoursesListComponent } from './admin-deg-req-courses-list.component';

describe('AdminDegReqCoursesListComponent', () => {
  let component: AdminDegReqCoursesListComponent;
  let fixture: ComponentFixture<AdminDegReqCoursesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDegReqCoursesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDegReqCoursesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
