import { Component, OnInit, Input, Optional } from '@angular/core';
import { Section } from '../section';
import { AdminServiceService } from '../admin-service.service';
import { DetailsComponent } from '../details/details.component';

@Component({
  selector: 'app-admin-sec-stud-list',
  templateUrl: './admin-sec-stud-list.component.html',
  styleUrls: ['./admin-sec-stud-list.component.css']
})
export class AdminSecStudListComponent implements OnInit {

  @Input() students = [];
  @Input() section : Section;

  errorMessage : string;
  error : boolean = false;

  remove(studentId){
    this.adminService.removeStudentFromSection(this.section.id, studentId).subscribe(
      res => {
        this.error = false;
        this.students = res.students;
        this.parent.section.capacity = this.parent.section.capacity + 1;
      });

  }

  addStudent(form){
    this.adminService.addStudentToSection(this.section.id, form.value.studentId).subscribe( 
      res => {
        this.error = false;
        this.students = res.students;
        this.parent.section.capacity = this.parent.section.capacity - 1;
      },
      err => {
        this.error = true;
        this.errorMessage = err.error.message;
      })
  }

  constructor(private adminService : AdminServiceService, @Optional() public parent : DetailsComponent) { }

  ngOnInit() {
  }

}