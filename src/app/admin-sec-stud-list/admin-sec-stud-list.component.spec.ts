import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSecStudListComponent } from './admin-sec-stud-list.component';

describe('AdminSecStudListComponent', () => {
  let component: AdminSecStudListComponent;
  let fixture: ComponentFixture<AdminSecStudListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSecStudListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSecStudListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
