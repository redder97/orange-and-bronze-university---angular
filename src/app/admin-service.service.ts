import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {

  apiUrlGetStudents: string = "http://localhost:8080/secure/admin/students";
  apiUrlGetSections: string = "http://localhost:8080/secure/admin/sections";

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization' : 'Bearer ' + sessionStorage.getItem('accessToken')
    })
  };

  getStudentList(): Observable<any>{
    return this.httpClient.get<any>(this.apiUrlGetStudents, this.httpOptions);
  }

  getSectionById(id): Observable<any>{
    return this.httpClient.get<any>(`http://localhost:8080/secure/admin/section/${id}`, this.httpOptions);
  }

  getSectionList(): Observable<any>{
    return this.httpClient.get<any>(this.apiUrlGetSections, this.httpOptions);
  }

  removeStudentFromSection(sectionId, studentId) : Observable<any>{
    return this.httpClient.post<any>(`http://localhost:8080/secure/section/${sectionId}/remove/${studentId}`, {},this.httpOptions );
  }

  addStudentToSection(sectionId, studentId): Observable<any>{
    return this.httpClient.post<any>(`http://localhost:8080/secure/section/${sectionId}/add/${studentId}`, this.httpOptions);
  }

  //Degree Services

  getDegrees(): Observable<any>{
    return this.httpClient.get<any>(`http://localhost:8080/secure/admin/degrees`, this.httpOptions);
  }

  getStudetsInDegree(id): Observable<any>{
    return this.httpClient.post<any>(`http://localhost:8080/secure/admin/degree/${id}/students`, this.httpOptions);
  }

  enlistStudent(degreeId, studentId): Observable<any>{
    return this.httpClient.post<any>(`http://localhost:8080/secure/degree/${degreeId}/student/${studentId}`, this.httpOptions);
  }

  removeStudentFromDegree(degreeId, studentId): Observable<any>{
    return this.httpClient.post<any>(`http://localhost:8080/secure/degree/${degreeId}/remove/${studentId}`, this.httpOptions);
  }

  getDegreeById(id): Observable<any>{
    return this.httpClient.get<any>(`http://localhost:8080/secure/degree/${id}`, this.httpOptions);
  }



  constructor(private httpClient : HttpClient) { }
}
