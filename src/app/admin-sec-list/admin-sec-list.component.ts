import { Component, OnInit, Input } from '@angular/core';
import { AdminServiceService } from '../admin-service.service';
import { Section } from '../section';

@Component({
  selector: 'app-admin-sec-list',
  templateUrl: './admin-sec-list.component.html',
  styleUrls: ['./admin-sec-list.component.css']
})
export class AdminSecListComponent implements OnInit {

  sections = [];

  @Input() viewSections : boolean;

  selectedSection : Section;

  getSections(){
    this.adminService.getSectionList().subscribe(res => res.map(sec => 
      this.sections.push(new Section(
        sec.id,
        sec.sectionName,
        sec.capacity,
        sec.term,
        sec.course,
        sec.days,
        sec.students
      ))))
  }

  viewDetails(section){
    this.adminService.getSectionById(section.id).subscribe(res => this.selectedSection = res);
  }

  constructor(private adminService : AdminServiceService) { }

  ngOnInit() {
    this.getSections();
  }

}
