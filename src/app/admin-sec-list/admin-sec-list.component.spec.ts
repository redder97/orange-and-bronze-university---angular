import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSecListComponent } from './admin-sec-list.component';

describe('AdminSecListComponent', () => {
  let component: AdminSecListComponent;
  let fixture: ComponentFixture<AdminSecListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSecListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSecListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
