import { Component, OnInit } from '@angular/core';
import { Student } from '../student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-student-dash',
  templateUrl: './student-dash.component.html',
  styleUrls: ['./student-dash.component.css']
})
export class StudentDashComponent implements OnInit {

  student: Student;

  getProfile(){
    this.studentService.getProfile().subscribe(res => 
      {this.student = new Student(
        res.id, 
        res.name,
        res.contactNumber,
        res.emailAddress,
        res.degree);
      console.log(this.student)},
      err => {
        console.log('error displaying profile');
      });
  }

  constructor(private studentService : StudentService) { }

  ngOnInit() {  
    this.getProfile();
    
    
  }

}
