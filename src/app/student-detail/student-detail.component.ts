import { Component, OnInit, Input } from '@angular/core';
import { Student } from '../student';

import { StudentService } from '../student.service';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.css']
})
export class StudentDetailComponent implements OnInit {

  @Input() student : Student;
  updateFormActive : boolean = false;

  submit(form){
    console.log(form.value);

    const details = {
      "emailAddress" : form.value.emailAddress,
      "contactNumber" : form.value.contactNumber
    }

    this.studentService.updateProfile(details, this.student.id).subscribe(res => this.student = res)
    this.toggleUpdateForm();
  }

  toggleUpdateForm(){
    this.updateFormActive = !this.updateFormActive;
  }

  constructor(private studentService : StudentService) { }

  ngOnInit() {
  }

}
