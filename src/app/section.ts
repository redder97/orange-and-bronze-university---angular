export class Section {
    id: number;
    sectionName: string;
    capacity: number;
    term: string;
    course: {};
    days: {};
    students: []; 
    
    constructor(id: number , sectionName: string, capacity: number, term: string, course: any, days: any, students: []){
        this.id = id;
        this.sectionName = sectionName;
        this.capacity = capacity;
        this.term = term;
        this.course = course;
        this.days = days;
        this.students = students;
    }
}
