import { Component, OnInit, Input, Optional } from '@angular/core';
import { AdminServiceService } from '../admin-service.service';
import { Degree } from '../degree';
import { Student } from '../student';
import { DetailsComponent } from '../details/details.component';

@Component({
  selector: 'app-admin-deg-stud-list',
  templateUrl: './admin-deg-stud-list.component.html',
  styleUrls: ['./admin-deg-stud-list.component.css']
})
export class AdminDegStudListComponent implements OnInit {  

  @Input() students = [];
  @Input() degreeId: number;

  errorMessage : string;
  error : boolean = false;
 
  addStudent(form){
    console.log(form.value.studentId);
    console.log(this.degreeId);

    this.adminService.enlistStudent(this.degreeId, form.value.studentId).subscribe(
      res => {
        this.error = false;
        this.students.push(new Student(
          res.id, res.name, 
          res.contactNumber, 
          res.emailAddress,   
          res.degree))
      },
      err => {
        this.error = true;
        this.errorMessage = err.error.message;
      }
    )
    
  }

  remove(id){
    this.adminService.removeStudentFromDegree(this.degreeId, id).subscribe(
      res => {console.log(res)
      this.error = false;
      this.students = this.students.filter(s => s.id != id);
      }
    )
  }

  constructor(private adminService : AdminServiceService, @Optional() private parent : DetailsComponent) {
  }

  ngOnInit() {
  }

}
