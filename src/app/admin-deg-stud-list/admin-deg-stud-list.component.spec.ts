import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDegStudListComponent } from './admin-deg-stud-list.component';

describe('AdminDegStudListComponent', () => {
  let component: AdminDegStudListComponent;
  let fixture: ComponentFixture<AdminDegStudListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDegStudListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDegStudListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
