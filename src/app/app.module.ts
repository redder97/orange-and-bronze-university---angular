import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AdminDashComponent } from './admin-dash/admin-dash.component';
import { AdminStudListComponent } from './admin-stud-list/admin-stud-list.component';
import { DetailsComponent } from './details/details.component';
import { AdminSecListComponent } from './admin-sec-list/admin-sec-list.component';
import { StudentDashComponent } from './student-dash/student-dash.component';
import { StudentDetailComponent } from './student-detail/student-detail.component';
import { AdminSecStudListComponent } from './admin-sec-stud-list/admin-sec-stud-list.component';
import { RegisterComponent } from './register/register.component';
import { AdminDegreeListComponent } from './admin-degree-list/admin-degree-list.component';
import { AdminDegStudListComponent } from './admin-deg-stud-list/admin-deg-stud-list.component';
import { AdminDegReqCoursesListComponent } from './admin-deg-req-courses-list/admin-deg-req-courses-list.component';
import { ErrorComponent } from './error/error.component';
import { LogoutComponent } from './logout/logout.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminDashComponent,
    AdminStudListComponent,
    DetailsComponent,
    AdminSecListComponent,
    StudentDashComponent,
    StudentDetailComponent,
    AdminSecStudListComponent,
    RegisterComponent,
    AdminDegreeListComponent,
    AdminDegStudListComponent,
    AdminDegReqCoursesListComponent,
    ErrorComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
