export class Degree {
    id: number;
    degreeName: string;
    requiredCourses: [];
    studentsInDegree: [];

    constructor(id: number, degreeName: string, requiredCourses: [], studentsInDegree: []){
        this.id = id;
        this.degreeName = degreeName;
        this.requiredCourses = requiredCourses;
        this.studentsInDegree = studentsInDegree;
    }

}
