import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  authErrorState: boolean = false;
  authErrorMessage: string;

  login(form){
    this.loginService.attemptLogin(form.value.username, form.value.password)
      .subscribe(res => {
          console.log("authentication success");
          sessionStorage.setItem('accessToken', res.accessToken);
          sessionStorage.setItem('tokenType', res.tokenType);
          
          if(res.auths[0].authority == "ROLE_ADMIN"){
            this.router.navigate(['/admin']);
          }else{
            this.router.navigate(['/student']);
          }
        },

        error => {
          console.log("authentication error");
          this.authErrorState = true;
          this.authErrorMessage = "Failed to login";
        }
      );
    
  }

  constructor(private loginService : LoginService, private router: Router) { }

  ngOnInit() {
  }

}
