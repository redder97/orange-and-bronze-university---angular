import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GuestService {

  register(newUser): Observable<any> {
    return this.httpClient.post<any>("http://localhost:8080/create/student", newUser);
  }

  constructor(private httpClient : HttpClient) { }
}
