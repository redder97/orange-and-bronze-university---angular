import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  signApiUrl: string = 'http://localhost:8080/signin';
   

  attemptLogin(username, password): Observable<any>{
    return this.httpClient.post(this.signApiUrl, {
      "username": username,
      "password": password
    });
  }

  constructor(private httpClient : HttpClient) { }
}

