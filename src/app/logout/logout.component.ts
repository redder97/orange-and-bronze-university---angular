import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  performLogout(){
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }
  

  constructor(private router : Router) { }


  ngOnInit() {
    this.performLogout();
  }

}
