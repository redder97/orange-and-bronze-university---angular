import { Component, OnInit } from '@angular/core';
import { Student } from '../student';
import { GuestService } from '../guest.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  newStudent: any;

  register(form){
    this.newStudent = {
      "name" : form.value.name,
      "username" : form.value.username,
      "password" : form.value.password,
      "contactNumber" : form.value.contactNumber,
      "emailAddress" : form.value.emailAddress
    }

    this.guestService.register(this.newStudent).subscribe(res => console.log(res));
  }

  constructor(private guestService : GuestService) { }

  ngOnInit() {
  }

}
