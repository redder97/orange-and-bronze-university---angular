import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../admin-service.service'

@Component({
  selector: 'app-admin-dash',
  templateUrl: './admin-dash.component.html',
  styleUrls: ['./admin-dash.component.css']
})
export class AdminDashComponent implements OnInit {

  adminToggleStudView: boolean;
  adminToggleSectView: boolean;
  adminToggleDegView: boolean;

  viewStudents(){
    this.adminToggleStudView = !this.adminToggleStudView;
    this.adminToggleSectView = false;
    this.adminToggleDegView = false;
  }

  viewSections(){
    this.adminToggleSectView = !this.adminToggleSectView;
    this.adminToggleDegView = false;
    this.adminToggleStudView = false;
  }

  viewDegrees(){
    this.adminToggleDegView = !this.adminToggleDegView;
    this.adminToggleSectView = false;
    this.adminToggleStudView = false;
  }

  constructor(private adminService : AdminServiceService) {
    this.adminToggleStudView = false;
    this.adminToggleSectView = false;
    this.adminToggleDegView = false;
  }

  ngOnInit() {
  }

}
