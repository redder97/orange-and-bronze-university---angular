export class Student {
    
    id: number;
    name: string;
    contactNumber: string;
    emailAddress: string;
    degree: string;

    constructor(id: number, name: string, contact: string, email: string, degree: string){
        this.id = id;
        this.name = name;
        this.contactNumber = contact;
        this.emailAddress = email;
        this.degree = degree;
    }



    //no need getter and setters unless you set them as private 

}
