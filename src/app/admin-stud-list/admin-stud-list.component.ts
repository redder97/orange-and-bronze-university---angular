import { Component, OnInit, Input } from '@angular/core';
import { AdminServiceService } from '../admin-service.service'
import { Student } from '../student';

@Component({
  selector: 'app-admin-stud-list',
  templateUrl: './admin-stud-list.component.html',
  styleUrls: ['./admin-stud-list.component.css']
})
export class AdminStudListComponent implements OnInit {

  students = [];
  
  selectedStudent : Student;

  getStudents(){

    console.log('pre: ' + sessionStorage.getItem("accessToken"));

    // this.adminService.getStudentList().subscribe(res => console.log());
    this.adminService.getStudentList().subscribe(res => res.map(obj => {
      this.students.push(new Student(
        obj.id,
        obj.name,
        obj.contactNumber,
        obj.emailAddress,
        obj.degree
      ))
    }));
  }

  viewStudentDetails(student: Student ){
    this.selectedStudent = student;
  }


  constructor(private adminService : AdminServiceService) { }

  ngOnInit() {
    this.getStudents();
  }

}
