import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminStudListComponent } from './admin-stud-list.component';

describe('AdminStudListComponent', () => {
  let component: AdminStudListComponent;
  let fixture: ComponentFixture<AdminStudListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminStudListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminStudListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
