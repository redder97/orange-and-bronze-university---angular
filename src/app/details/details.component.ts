import { Component, OnInit, Input } from '@angular/core';
import { Student } from '../student';
import { Section } from '../section';
import { Degree } from '../degree';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  @Input() student : Student;
  @Input() section : Section;
  
  @Input() degree : Degree;

  constructor() { }

  ngOnInit() {
  }

}
