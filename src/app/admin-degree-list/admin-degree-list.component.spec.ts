import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDegreeListComponent } from './admin-degree-list.component';

describe('AdminDegreeListComponent', () => {
  let component: AdminDegreeListComponent;
  let fixture: ComponentFixture<AdminDegreeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDegreeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDegreeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
