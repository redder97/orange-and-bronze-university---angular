import { Component, OnInit, Optional } from '@angular/core';
import { AdminServiceService } from '../admin-service.service';
import { Degree } from '../degree';

@Component({
  selector: 'app-admin-degree-list',
  templateUrl: './admin-degree-list.component.html',
  styleUrls: ['./admin-degree-list.component.css']
})
export class AdminDegreeListComponent implements OnInit {

  degrees = [];
  selectedDegree : Degree;


  getDegrees(){
    this.adminService.getDegrees().subscribe(res => {
      res.map(degree => {
        this.degrees.push(  
          new Degree(
            degree.id, 
            degree.degreeName,
            degree.requiredCourses,
            degree.studentsInDegree == null ? [] : degree.studentsInDegree)
        )
      });
    },
    err => {
      console.log("error retrieving degrees");
    } )
  }

  viewDegreeDetails(degree){
    this.adminService.getDegreeById(degree.id).subscribe(res => {
      this.selectedDegree = new Degree(
        res.id,
        res.degreeName,
        res.requiredCourses,
        res.studentsInDegree == null ? [] : res.studentsInDegree

      );
    })
  }

  constructor(private adminService : AdminServiceService) { }

  ngOnInit() {
    this.getDegrees();
  }

}
