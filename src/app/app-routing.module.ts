import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminDashComponent } from './admin-dash/admin-dash.component';
import { LoginComponent } from './login/login.component';
import { StudentDashComponent } from './student-dash/student-dash.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';


const routes: Routes = [
  { path: 'admin', component: AdminDashComponent },
  { path: 'login', component: LoginComponent},
  { path: 'student', component: StudentDashComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'logout', component: LogoutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
